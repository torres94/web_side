document.querySelector('.mnu-btn').addEventListener('click',() => {
    document.querySelector('.nav-menu').classList.toggle('show')
})


ScrollReveal().reveal('.showcase',{delay: 500});
ScrollReveal().reveal('.news-cards',{delay: 800});
ScrollReveal().reveal('.social',{delay: 800});
ScrollReveal().reveal('.cards-banner-one',{delay: 800});
ScrollReveal().reveal('.cards-banner-two',{delay: 800});
ScrollReveal().reveal('.footer-links',{delay: 800});


$(document).ready(function(){
    $('.arriba').click(function(){
        $('body, html').animate({
            scrollTop: '0px'
        }, 600);
    });

    $(window).scroll(function(){
        if($(this).scrollTop() > 0){
            $('arriba').slideDown(300);
        }else{
            $('arriba').slideUp(300);
        }
    })
})